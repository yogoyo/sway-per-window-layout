// Using string_cache to intern layout names and input identifiers
use string_cache::DefaultAtom as Atom;
use swayipc::{
    Connection, Error, EventType,
    reply::{ WindowChange, Event },
    Fallible
};

use std::collections::HashMap;

fn save_layout(ipc: &mut Connection) -> Fallible<HashMap<Atom, Atom>> {
    //eprintln!("get_inputs(): {:#?}", ipc.get_inputs()?);
    Ok(ipc.get_inputs()?
       .into_iter()
       .filter_map(|input| {
           let identifier: Atom = input.identifier.into();
           input.xkb_active_layout_name.map(|layout| (identifier, layout.into()))
       })
       .collect())
}

fn restore_layout(ipc: &mut Connection, saved: &HashMap<Atom, Atom>) {
    // best-effort
    for input in ipc.get_inputs().unwrap_or_else(|e| {eprintln!("Failed to get active inputs: {:?}", e); vec![]}) {
        let identifier = input.identifier.into();
        if let Some(saved_layout) = saved.get(&identifier) {
            //get the correct index for the layout
            if let Some(idx) = input.xkb_layout_names.iter().position(|x| x.as_str() == saved_layout) {
                if let Err(e) = ipc.run_command(format!("input \"{}\" xkb_switch_layout {}", identifier, idx)) {
                    eprintln!("E: Failed to update input {:?} to saved layout {:?} ({:?}): {:?}", identifier, saved_layout, idx, e);
                }
            } else {
                eprintln!("E: No such layout for input {:?}: {:?} (available {:?})", identifier, saved_layout, input.xkb_layout_names);
            }
        }
    }
}

fn main() -> Result<(), Error> {
    let mut saved_layouts: HashMap<i64, HashMap<Atom, Atom>> = HashMap::new();
    let mut ipc = Connection::new()?;

    let mut prev = ipc.get_tree().map(|t| t.find_as_ref(|n| n.focused).map(|w| w.id)).ok().flatten();

    eprintln!("starting event loop");

    for event in Connection::new()?.subscribe(&[EventType::Window])? {
        if let Ok(Event::Window(event)) = event {
            match event.change {
                WindowChange::Focus => {
                    if let Some(prev) = prev {
                        match save_layout(&mut ipc) {
                            Ok(layout) =>{ saved_layouts.insert(prev, layout); },
                            Err(e) => eprintln!("E: Failed to save layout for window: {:?}", e)
                        }
                    }
                    if let Some(saved) = saved_layouts.get(&event.container.id) {
                        restore_layout(&mut ipc, saved);
                    } 
                    prev = Some(event.container.id);
                },
                WindowChange::Close => {
                    saved_layouts.remove(&event.container.id);

                    if let Some(saved) = prev {
                        if saved == event.container.id {
                            prev = None;
                        }
                    }
                }
                _ => ()
            }
        }
    }
    Ok(())
}
